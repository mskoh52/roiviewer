var bcrypt = require('bcrypt-nodejs');
var bodyParser = require('body-parser');
var cookieSession = require('cookie-session');
var express = require('express');
var fs = require('fs');
var https = require('https');
var path = require('path');
var session = require('express-session');

var hashed_pass = '$2a$10$FgThzscoKU/coIZ/JYzHr.YgIdBImjmdopSChZ2u1xuZzoWlVuG4S';

var app = express();

var debug = true;

function authenticate (req, res, next) {
    if (debug) console.log('Authentication check: '+req.session.auth);
    if (req.session.auth) {
        next();
    } 
    else {
        res.sendFile(path.resolve(__dirname+'/../html/auth.html'));
    }
}

app.use(bodyParser.urlencoded( {extended: false} ));

app.use(session({
    name: 'session',
    /*
    secret: require('crypto').randomBytes(64, function (ex, buf) {
        if (ex) throw ex;
        return buf
    }),*/
    secret: 'asdweivxzdiq39wdkmfghjklkjgoiwjfawweriweiq',
    resave: false, 
    saveUninitialized: true, 
    cookie: { maxAge: 600000 }
}));

// Serve the robots
app.get('/robots.txt', function (req, res) {
    res.sendFile(path.resolve(__dirname+'/../robots.txt'));
});

if (debug) {
    app.use(function (req, res, next) {
        console.log('---'+req.originalUrl+'---');
        //console.log(req.session);
        next();
    });
}

// Static routes for resources
app.use('/expts', authenticate, express.static(path.join(__dirname,  '/../expts')));
app.use('/js', authenticate, express.static(path.join(__dirname,  '/../js')));
app.use('/css', authenticate, express.static(path.join(__dirname,  '/../css')));

// Index page, check for authentication
app.get('/', authenticate, function (req, res) {
    res.redirect('/experiment_viewer')
});

app.post('*', function (req, res) {
    if (debug) console.log('post');
    bcrypt.compare(req.body.password, hashed_pass, function (err, result) {
        if (result) {
            req.session.auth = true;
            if (debug) console.log('correct password!');
            console.log(req.originalUrl);
            res.redirect(req.originalUrl);
            //res.redirect('/experiment_viewer');
        } else {
            req.session.auth = false;
            if (debug) console.log('incorrect password!');
        }
    } );
});

app.get('/experiment_viewer', authenticate, function (req, res) {
    res.sendFile(path.resolve(__dirname+'/../html/experiments.html'));
});
app.get('/roi_viewer.html', authenticate, function (req, res) {
    res.sendFile(path.resolve(__dirname+'/../html/roi_viewer.html'));
});

https.createServer({
    key: fs.readFileSync(path.join(__dirname, 'key.pem')),
    cert: fs.readFileSync(path.join(__dirname, 'cert.pem'))
}, app).listen(8081);
