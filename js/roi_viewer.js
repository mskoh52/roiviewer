//
var res_prefix = '/';
// Parse query string
var queryvars = (function (querystring) {
    var dict = {};
    for (q of querystring.split('&')) {
        var s = q.split('=', 2);
        if (s.length == 1)
            dict[s[0]] = "";
        else
            dict[s[0]] = s[1];
    }
    return dict;
}) (window.location.search.substr(1));

var tree = (function () {
    var tree = {};
    var treetmp = (function () {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': res_prefix + 'expts/' + queryvars['expt'] + '/roitree.json',
            'dataType': 'json',
            'success': function (data) {
                json = data;
            }
        });
        return json;
    })();

    var fovnames = $.map(treetmp, function(value, key) { return key; });
    fovnames.sort();
    for (fov of fovnames) {
        tree[fov] = {};
        var setnames = $.map(treetmp[fov], function(value, key) { return key; });
        setnames.sort();
        for (set of setnames) {
            treetmp[fov][set].sort();
            tree[fov][set] = treetmp[fov][set].sort();
        }
    }
    return tree;
})();

function update_fov(fov) {
    var fovname = fov.text();
    var setname = update_set.current_set.text()
    var roinum = update_roi.current_roi.text()
    var newsrc = fovname + "_" + setname + "_roi" + roinum;
    document.getElementById("img_fovimg").src= res_prefix + "expts/" + queryvars['expt'] + '/' + newsrc + "_fovimg.png";
    document.getElementById("img_roiimg").src= res_prefix + "expts/" + queryvars['expt'] + '/' + newsrc + "_roiimg.png";
    document.getElementById("img_traces").src= res_prefix + "expts/" + queryvars['expt'] + '/' + newsrc + "_traces.png";

    if (typeof update_fov.current_fov === 'undefined')
        update_fov.current_fov = $("a.fov:first");
    update_fov.current_fov.css("background-color", "#CCCC99");
    fov.css("background-color", "#FFFFAA");
    update_fov.current_fov = fov;
    $("#setselect").html("");
    for (var set in tree[fovname]) {
        $("#setselect").append( "<a href='#' class='set'>"+set+"</a><br>" );
    }
}

function update_set(set) {
    var setname = set.text();
    var fovname = update_fov.current_fov.text()
    var roinum = update_roi.current_roi.text()
    var newsrc = fovname + "_" + setname + "_roi" + roinum;
    document.getElementById("img_fovimg").src= res_prefix + "expts/" + queryvars['expt'] + '/' + newsrc + "_fovimg.png";
    document.getElementById("img_roiimg").src= res_prefix + "expts/" + queryvars['expt'] + '/' + newsrc + "_roiimg.png";
    document.getElementById("img_traces").src= res_prefix + "expts/" + queryvars['expt'] + '/' + newsrc + "_traces.png";

    document.getElementById("img_roimap").src= res_prefix + "expts/" + queryvars['expt'] + '/' + fovname + "_" + setname + "_roimap.png";
    document.getElementById("img_InhibitionVsArea").src= res_prefix + "expts/" + queryvars['expt'] + '/' + fovname + "_" + setname + "_InhibitionVsArea.png";
    document.getElementById("img_stimrois").src= res_prefix + "expts/" + queryvars['expt'] + '/' + fovname + "_" + setname + "_stimrois.png";

    if (typeof update_set.current_set === 'undefined')
        update_set.current_set = $("a.set:first");

    update_set.current_set.css("background-color", "#CCCC99");
    set.css("background-color", "#FFFFAA");
    update_set.current_set = set;
    $("#roiselect").html("");
    for (var roi of tree[fovname][setname]) {
        $("#roiselect").append( " <a href='#' class='roi'>"+roi+"</a> " );
    }
    var previously_selected_roi = $("a.roi").filter(function (index) { return $(this).text() === update_roi.current_roi.text(); });
    if (previously_selected_roi.length === 0) {
        previously_selected_roi = $("a.roi:first");
    }
    update_roi(previously_selected_roi);
}

function update_roi(roi) {
    var roinum = roi.text();
    var fovname = update_fov.current_fov.text()
    var setname = update_set.current_set.text()
    var newsrc = fovname + "_" + setname + "_roi" + roinum;
    document.getElementById("img_fovimg").src= res_prefix + "expts/" + queryvars['expt'] + '/' + newsrc + "_fovimg.png";
    document.getElementById("img_roiimg").src= res_prefix + "expts/" + queryvars['expt'] + '/' + newsrc + "_roiimg.png";
    document.getElementById("img_traces").src= res_prefix + "expts/" + queryvars['expt'] + '/' + newsrc + "_traces.png";
    document.getElementById("img_stackedRoiTracesP").src= res_prefix + "expts/" + queryvars['expt'] + '/' + newsrc + "_stackedRoiTracesP.png";

    if (typeof update_roi.current_roi === 'undefined')
        update_roi.current_roi = $("a.roi:first");

    update_roi.current_roi.css("background-color", "#CCCC99");
    roi.css("background-color", "#FFFFAA");
    update_roi.current_roi = roi;
}

$(document).ready(function() {

    for (var fov in tree) {
        $("#fovselect").append( "<a href='#' class='fov'>"+fov+"</a><br>" );
    }
    update_fov.current_fov = $("a.fov:first");
    for (var set in tree[update_fov.current_fov.text()]) {
        $("#setselect").append( "<a href='#' class='set'>"+set+"</a><br>" );
    }
    update_set.current_set = $("a.set:first");
    for (var roi of tree[update_fov.current_fov.text()][update_set.current_set.text()]) {
        $("#roiselect").append( " <a href='#' class='roi'>"+roi+"</a> " );
    }
    update_roi.current_roi = $("a.roi:first");

    // Load the first entries
    update_fov( $("a.fov:first") );
    update_set( $("a.set:first") );
    update_roi( $("a.roi:first") );
    // Bind click listeners
    $("#fovselect").on('click', 'a.fov', function() {
        update_fov( $(this) );
    });
    $("#setselect").on('click', 'a.set', function() {
        update_set( $(this) );
    });
    $("#roiselect").on('click', 'a.roi', function() {
        update_roi( $(this) );
    });

    // Add keymaps for left/right arrow
    $("body").on('keydown', function (e) {
        if (e.keyCode == 39) {
            update_roi( update_roi.current_roi.nextAll("a:first") );
        }
        else if (e.keyCode == 37) {
            update_roi( update_roi.current_roi.prevAll("a:first") );
        }
        else {
            return;
        }
    });

});
